# git for dummies
###	This repo is primarily intended as a dummy project for familiarizing with git.

#  How to use this repo :
* Fork the repo
* Clone the forked repo and make the following changes in local copy
* Inside utils create a file `|yourname|.py` .(replace `|yourname|` with your real name)
* Inside `|yourname|.py` define a function `who_r_u()` which prints some string of your choice
* Edit the `run.py` file and change line `from utils.prince import *` to 'from utils.|yourname| import *' (replace |yourname| with your real name) 
* Run `python run.py` inside git-for-dummies
* If everything goes right you should see the string of your choice printed in terminal.
* If so commit and push your local repo to your remote master branch
* send a merge request from your master branch to my master branch and wait for me to accept.

# How the code works :
### execute the following command on your terminal
```
git clone https://gitlab.com/9rince/git-for-dummies.git

cd git-for-dummies

python run.py
```
### output
```
A Dream Within a Dream
        By Edgar Allan Poe
Take this kiss upon the brow!
And, in parting from you now,
Thus much let me avow —
You are not wrong, who deem
That my days have been a dream;
Yet if hope has flown away
In a night, or in a day,
In a vision, or in none,
Is it therefore the less gone? 
All that we see or seem
Is but a dream within a dream.

I stand amid the roar
Of a surf-tormented shore,
And I hold within my hand
Grains of the golden sand —
How few! yet how they creep
Through my fingers to the deep,
While I weep — while I weep!
O God! Can I not grasp
Them with a tighter clasp?
O God! can I not save
One from the pitiless wave?
Is all that we see or seem
But a dream within a dream?
```